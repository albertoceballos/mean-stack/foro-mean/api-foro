'use strict'

var Topic = require('../models/topic');
var validator = require('validator');

var controller = {

    test: function (req, res) {
        return res.status(200).send({ message: 'hola desde el controlador de Comentarios' });
    },

    //añadir comentario a un topic
    add: function (req, res) {
        //recoger el Id del topic
        var topicId = req.params.id;
        //recoger comenatrio del body
        var params = req.body;
        //find por ID
        Topic.findById(topicId, (err, topic) => {
            if (err) return res.status(500).send({ message: 'Error en la petición ', status: 'error' })
            if (!topic) {
                return res.status(404).send({ message: 'No existe el tema', status: 'error' })
            }
            //validar datos:

            try {
                var validator_content = !validator.isEmpty(params.content);
            } catch (error) {
                return res.status(500).send({ message: 'Faltan datos por enviar', status: 'error' });
            }

            if (validator_content) {
                var comment = {
                    user: req.user.sub,
                    content: params.content,
                }
                //añadir comentario al topic
                topic.comments.push(comment);
                //guardar topic
                topic.save((error) => {
                    if (error) return res.status(500).send({ message: 'Error al actualizar el topic', status: 'error' });
                    return res.status(200).send({ message: 'Comentario añadido con éxito', status: 'success', topic: topic });
                });

            } else {
                return res.status(500).send({ message: 'No has comentado nada!', status: 'error' });
            }


        });


    },
    update: function (req, res) {
        //recoger id del commentario
        var commentId = req.params.id;
        //recoger datos del body
        var params = req.body;
        //validar datos:
        try {
            var validator_content = !validator.isEmpty(params.content);
        } catch (error) {
            return res.status(500).send({ message: 'Faltan datos por enviar', status: 'error' });
        }
        if(validator_content){
            //find an update
            Topic.findOneAndUpdate({'comments._id':commentId},{'$set':{'comments.$.content':params.content}},{new:true},(err,topicUpdated)=>{
                if(err) return res.status(500).send({message:'Error al actualizar comentario',status:'error'}); 
                if(!topicUpdated){
                    return res.status(404).send({message:'No existe el comentario',status:'error'}); 
                }else{
                    return res.status(200).send({message:'Comentario actualizado con éxito',status:'success',topic:topicUpdated});
                }
                
            });


        }else{
            return res.status(500).send({ message: 'Introduce un comentario', status: 'error' });
        }
    },

    delete:function(req,res){
        //recoge id del topic:
        var topicId=req.params.topicId;
        //recoge ide del comentario:
        var commentId=req.params.commentId;

        //buscar el topic
        Topic.findById(topicId,(error,topic)=>{
            if(error) return res.status(500).send({message:'Error en la petición',status:'error'}); 
            if(!topic){
                return res.status(500).send({message:'No existe el tema',status:'error'}); 
            }

            //si encuentra el topic, busco el comentario en el topic:
            var comment=topic.comments.id(commentId);
            //borro el comentario
            if(comment){
                comment.remove();
                topic.save((err)=>{
                    if(err) return res.status(500).send({ message: 'Error al actualizar el topic', status: 'error' });
                    return res.status(200).send({message:'Comentario borrado con éxito',status:'success', topic:topic});
                });
                
            }else{
                return res.status(500).send({message:'No existe el comentario',status:'error'}); 
            }

        });
    }

};

module.exports = controller;