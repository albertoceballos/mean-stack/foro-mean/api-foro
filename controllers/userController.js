'use strict'

var validator = require('validator');
var User = require('../models/user');
var bcrypt = require('bcrypt-nodejs');
var fs=require('fs');
var path=require('path');

//
var jwt = require('../services/jwt');

var controller = {

    probando: function (req, res) {
        return res.status(200).send({ message: "hola desde el controlador User" });
    },
    save: function (req, res) {
        //recoger los parámetros
        var params = req.body;
        try {
            var validate_name = !validator.isEmpty(params.name);
            var validate_surname = !validator.isEmpty(params.surname);
            var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
            var validate_password = !validator.isEmpty(params.password);
        } catch (error) {
            return res.status(200).send({ message: 'Faltan datos' });
        }
        //si la validación es buena
        if (validate_name && validate_surname && validate_email && validate_password) {
            var user = new User();
            user.name = params.name;
            user.surname = params.surname;
            user.email = params.email;
            user.image = null;
            user.role = 'ROLE_USER';

            //comprobar si existe el usuario
            User.findOne({ email: user.email }, (err, issetUser) => {
                if (err) {

                    return res.status(500).send({ message: 'Error al comprobar el usuario' });
                }
                if (!issetUser) {
                    //cifrar contraseña:
                    bcrypt.hash(params.password, null, null, (error, hash) => {
                        user.password = hash;

                        //guardar usuario
                        user.save((err, userStored) => {
                            if (err) {
                                return res.status(500).send({ message: 'Error al guardar el usuario' });
                            }
                            if (!userStored) {
                                return res.status(500).send({ message: 'El usuario no se ha guardado' });
                            }
                            return res.status(200).send({ message: 'Usuario registrado con éxito', user: userStored, status: 'success' });
                        });
                    });

                } else {
                    return res.status(500).send({ message: 'Ya existe el usuario' });
                }
            });


        } else {
            return res.status(200).send({ message: "Validación de usuarios incorrecta" });
        }


    },

    login: function (req, res) {
        //recoger parámetros:
        var params = req.body;
        var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
        var validate_password = !validator.isEmpty(params.password);

        if (validate_email && validate_password) {
            //buscar el usuario:
            User.findOne({ email: params.email.toLowerCase() }, (err, userLogin) => {
                if (err) return res.status(500).send({ message: 'Error al buscar el usuario' });
                //si existe el usuario:
                if (userLogin) {
                    //generar token jwt
                    if (params.getToken) {
                        return res.status(200).send({ token: jwt.createToken(userLogin) });
                    } else {
                        //comparar password con bcrypt
                        bcrypt.compare(params.password, userLogin.password, (error, result) => {
                            if (result) {
                                //limpiar password para que no lo devuelva
                                userLogin.password = undefined;
                                return res.status(200).send({ status: 'success', user: userLogin });
                            } else {
                                return res.status(200).send({ message: 'Las credenciales no son correctas' });
                            }

                        });
                    }

                } else {
                    return res.status(404).send({ message: 'No existe el usuario' });
                }
            });
        } else {
            return res.status(200).send({ message: 'Los datos son incorrectos' });
        }
    },

    update: function (req, res) {
        //recoger parámetros
        var params = req.body;
        //validar paramétros enviados
        try {
            var validate_name = !validator.isEmpty(params.name);
            var validate_surname = !validator.isEmpty(params.surname);
            var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);

        } catch (error) {
            return res.status(200).send({ message: 'Faltan datos' });
        }

        if (validate_name && validate_surname && validate_email) {
            var userId = req.user.sub;

            //Coimprobar si el email es único
            if (req.user.email != params.email) {
                User.findOne({email:params.email},(err,user)=>{
                     if(err)  return res.status(500).send({message:"Error al intentar identificarse"});

                     if(user && user.email==params.email){
                        return res.status(200).send({message:"El email no puede ser modificado"});
                     }else{
                        User.findOneAndUpdate({ _id: userId }, params, { new: true }, (err, userUpdated) => {
                            if (err) return res.status(404).send({ message: 'No existe el usuario', status: 'error' });
        
                            if (!userUpdated) return res.status(500).send({ message: 'Error al actualizar usuario', status: 'error' });
        
                            return res.status(200).send({ message: 'usuario actualizado', status: 'succes', user: userUpdated });
        
                        });

                     }
                });

            }else{
                //Buscar el usuario para actualizarlo
                User.findOneAndUpdate({ _id: userId }, params, { new: true }, (err, userUpdated) => {
                    if (err) return res.status(404).send({ message: 'No existe el usuario', status: 'error' });

                    if (!userUpdated) return res.status(500).send({ message: 'Error al actualizar usuario', status: 'error' });

                    return res.status(200).send({ message: 'usuario actualizado', status: 'succes', user: userUpdated });

                });
            }
        } else {
            return res.status(200).send({ message: 'Los datos no son válidos' });
        }

    },

    //subir avtar:
    uploadAvatar:function(req,res){
        if(!req.files){
            return res.status(404).send({message:'Avatar no subido',status:'error'});
        }
        var file_path=req.files.file0.path;
        var file_split=file_path.split('\\');
        
        var file_name=file_split[2];

        var file_name_split=file_name.split('.');
        var file_extension=file_name_split[1];

        //console.log(file_split);
        //console.log(file_name);
        //console.log(file_extension);
       
        


        if(file_extension!='jpg' && file_extension!='jpeg' && file_extension!='gif' && file_extension!='png'){
            fs.unlink(file_path,(err)=>{
                return res.status(200).send({message:'La extensión de archivo no es válida',status:'error'});
            });
        }else{
            //Sacar el id del usuario:
            var userId=req.user.sub;

            //Buscar y actualizar el documento:
            User.findOneAndUpdate({_id:userId},{image:file_name},{new:true},(err,userUpdated)=>{

                if(err  || !userUpdated){
                    return res.status(500).send({message:'Error al guardar el usuario',status:'error'});
                }else{
                    return res.status(200).send({message:'Avatar súbido',status:'success',user:userUpdated});
                }

                
            });

            
        }     
    },

    //conseguir avatar:
    getAvatar:function(req,res){
        var file_name=req.params.fileName;
        var file_path='./uploads/users/'+file_name;
        fs.exists(file_path,(exists)=>{
            if(exists){
                 res.sendFile(path.resolve(file_path));
            }else{
                 res.status(404).send({message:'No existe la imagen'});
            }
        });

    },

    //Conseguir todos los usuarios
    getUsers:function(req,res){
        User.find({},(err,users)=>{
            if(err) return res.status(500).send({message:'Error al buscar usuarios',status:'error'});
            if(!users){
                return res.status(404).send({message:'No hay usuarios',status:'error'});
            }else{
                return res.status(200).send({status:'success',users:users});
            }
        });
    },

    //Conseguir un usuario concreto
    getUser:function(req,res){
        var userId=req.params.userId;
        User.findById(userId,(error,user)=>{
            if(error) return res.status(500).send({message:'Error al buscar usuario',status:'error'});
            if(!user){
                return res.status(404).send({message:'No existe el usuario',status:'error'});
            }else{
                return res.status(200).send({status:'success',user:user});
            }
        });

    },

}


module.exports = controller;