'use strict'
var jwt=require('jwt-simple');
var secret='clave-generar-token999000';
var moment=require('moment');

exports.auth=function(req,res,next){

   
    //comprobar si llega la autenticación
    if(!req.headers.authorization){
        return res.status(200).send({message:'No hay cabecera de autenticación'});
    }
    var token=req.headers.authorization.replace(/['"]+/g,'');

    try{
        //decodificar token
        var payload=jwt.decode(token,secret);

        //comprobar si ha expirado
        if(payload.exp<=moment().unix()){
            return res.status(404).send({message:'El token ha expirado'});
        }


    }catch(ex){
        return res.status(404).send({message:'El token no es válido'});
    }

    req.user=payload;

    next();
};