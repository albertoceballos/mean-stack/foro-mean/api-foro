'use strict'

//Conexión a Mongoose
var mongoose=require('mongoose');
var app=require('./app');
var port=process.env.PORT || 3999;

mongoose.Promise=global.Promise;
mongoose.connect('mongodb://localhost:27017/foro-MEAN', {useNewUrlParser:true}).then(()=>{
    console.log("La conexión ala BBDD se ha realizado correctamente");
    
    //crear servidor
    app.listen(port,()=>{
        console.log("El servidor Express está funcionando en http.//localhost:3999");
    });
})
.catch((error)=>{
    console.log(error);
});

